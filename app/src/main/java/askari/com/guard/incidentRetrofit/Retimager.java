package askari.com.guard.incidentRetrofit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import askari.com.guard.Activities.LoginActivity;
import askari.com.guard.Utilities.Constant;
import askari.com.guard.Utilities.SessionManager;
import askari.com.guard.incidentRetrofit.model.ApiModel;
import askari.com.guard.incidentRetrofit.network.ApiConstants;
import askari.com.guard.incidentRetrofit.network.ServiceInterface;
import askari.com.guard.incidentRetrofit.utils.FileUtil;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import askari.com.guard.R;
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class Retimager extends AppCompatActivity {

    ImageView selectedImage,test,test1;
    Bitmap bitmap;
    String encodedimage;
    CircularProgressButton btnSubmit;
    ServiceInterface serviceInterface;
    List<Uri> files = new ArrayList<>();
    SessionManager session;
    String guard_id, name, longitude, latitude, batlev,androidID;
    private LinearLayout parentLinearLayout;

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        parentLinearLayout= findViewById(R.id.parent_linear_layout);

        session = new SessionManager(Retimager.this);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> users = session.getUserDetails();
        guard_id = users.get(session.KEY_ID);
        name = users.get(session.KEY_NAME);

        ImageView addImage = findViewById(R.id.iv_add_image);
        test = findViewById(R.id.test);
        test1 = findViewById(R.id.test1);

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage1();
            }
        });

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addImage();
            }
        });

        btnSubmit = findViewById(R.id.submit_button);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission();
                retima();
            }
        });


    }
    //===== add image in layout
    public void addImage1() {
        selectedImage = findViewById(R.id.test);
        selectImage(Retimager.this);
    }

    //===== add image in layout
    public void addImage() {
        LayoutInflater inflater=(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView=inflater.inflate(R.layout.image, null);
        // Add the new row before the add field button.
        parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
        parentLinearLayout.isFocusable();

        selectedImage = rowView.findViewById(R.id.number_edit_text);
        selectImage(Retimager.this);
    }

    //===== select image
    private void selectImage(Context context) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Choose a Media");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);//one can be replaced with any action code

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {

                         bitmap =(Bitmap)data.getExtras().get("data");
                        selectedImage.setImageBitmap(bitmap);
                        encodebitmap(bitmap);
                        Picasso.get().load(getImageUri(Retimager.this,bitmap)).into(test1);

                        String imgPath = FileUtil.getPath(Retimager.this,getImageUri(Retimager.this,bitmap));

                        files.add(Uri.parse(imgPath));
                        Log.e("image", imgPath);
                    }

                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri img = data.getData();
                        Picasso.get().load(img).into(test);

                        try{
                            InputStream inputStream = getContentResolver().openInputStream(img);
                            bitmap = BitmapFactory.decodeStream(inputStream);
                            encodebitmap(bitmap);

                        }catch (FileNotFoundException e){
                            e.printStackTrace();
                        }

                        String imgPath = FileUtil.getPath(Retimager.this, img);

                        files.add(Uri.parse(imgPath));
                        Log.e("image", imgPath);

                    }
                    break;
            }
        }
    }
    private void encodebitmap(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);

        byte[] byteofimages=byteArrayOutputStream.toByteArray();
        encodedimage=android.util.Base64.encodeToString(byteofimages, Base64.DEFAULT);
    }

    //===== bitmap to Uri
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "askari", null);
        Log.d("image uri",path);

        byte[] byteofimages =bytes.toByteArray();
        encodedimage=android.util.Base64.encodeToString(byteofimages, Base64.DEFAULT);
        return Uri.parse(path);
//        return encodedimage;
    }

    //===== Upload files to server
    private void retima() {
    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.RET,
            new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String ServerResponse) {
                    //showMe.dismiss();
                    JSONObject j = null;
                    try {
                        j = new JSONObject(ServerResponse);

                        String status = j.getString("status");
                        if (status.equals("200")) {


                            String title = j.getString("msg");
//                                String descripts = j.getString("description");
//                                String shiftTime = j.getString("shift");
//                                String intimee = j.getString("intime");
//                                String tround = j.getString("total_rounds");
//                                String cround = j.getString("completed_rounds");
//                                String mround = j.getString("missed_checkpoints");
//                                endtime = j.getString("end");

                            Toast.makeText(Retimager.this,title, Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(Retimager.this, LoginActivity.class);
                            startActivity(intent);
                            Retimager.this.finish();
//                                descript.setText(descripts);
//                                duration.setText(shiftTime);
//                                intime.setText("IN time: " + intimee);
//                                total.setText(tround);
//                                complete.setText(cround);
//                                check.setText(mround);
//                                //  showMe.dismiss();
//
//                                mShimmerViewContainer.stopShimmerAnimation();
//                                mShimmerViewContainer.setVisibility(View.GONE);
//                                companyrr.setVisibility(View.VISIBLE);

                        }else if(status.equals("500")) {
                            String m = j.getString("msg");
//                                session.logoutUser();
//                                getActivity().finish();
//                                Intent i = new Intent(getActivity(), LoginActivity.class);
//                                startActivity(i);
                            Toast.makeText(Retimager.this, m, Toast.LENGTH_SHORT).show();

                        }
                        else {
                            String failed = j.getString("msg");
                            // showMe.dismiss();
                            Toast.makeText(Retimager.this, failed, Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                }
            },
            new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                    // showMe.dismiss();
                    if (Retimager.this!= null) {
                        Log.d("volleyError",""+volleyError);
                        Toast.makeText(Retimager.this, "Wrong password", Toast.LENGTH_SHORT).show();
                    }

                    //Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                }
            }) {


        @Override
        public Map<String, String> getHeaders() {
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
            return headers;
        }

        @Override
        protected Map<String, String> getParams() {

            // Creating Map String Params.
            Map<String, String> params = new HashMap<String, String>();

//            for (Uri uri:files) {
//
//                Log.i("uris",uri.getPath());
//
//
//
//
//            }



            params.put("file", encodedimage);
            params.put("guard_id",guard_id);

            return params;
        }
    };

    RequestQueue requestQueue = Volley.newRequestQueue(Retimager.this);
        requestQueue.add(stringRequest);
}

   @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {

        File file = new File(fileUri.getPath());
        Log.i("here is error",file.getAbsolutePath());
        // create RequestBody instance from file

            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse("image/*"),
                            file);

            // MultipartBody.Part is used to send also the actual file name
            return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);


    }


    // this is all you need to grant your application external storage permision
    private void requestPermission(){
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(Retimager.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){
            case REQUEST_CODE_ASK_PERMISSIONS:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    retima();
                }
                else {
                    Toast.makeText(Retimager.this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }
}
