package askari.com.guard.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;
import askari.com.guard.Activities.DashboardActivity;
import askari.com.guard.R;
import askari.com.guard.Utilities.SessionManager;

public class ProfileFragment extends Fragment {

    TextView gname, gphone, gemail, gagency, gid;
    private CircleImageView userProfileImage;
    SessionManager session;
    String id,name,number,email,agency,guardid,userImage;
    RelativeLayout back;
    LinearLayout ll;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fabric.with(getActivity(), new Crashlytics());
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        session = new SessionManager(getActivity());
        HashMap<String, String> users = session.getUserDetails();
      //  id = users.get(session.KEY_ID);


        name = users.get(session.KEY_NAME);
        email = users.get(session.KEY_EMAIL);
        number = users.get(session.KEY_NUMBER);
        agency = users.get(session.KEY_AGENCY);
        guardid = users.get(session.KEY_GUARDID);
        userImage= users.get(session.KEY_PROFILEIMAGE);

        userProfileImage = (CircleImageView) v.findViewById(R.id.visit_profile_image);
        gid = (TextView) v.findViewById(R.id.idnumber);
        gname = (TextView) v.findViewById(R.id.name);
        gphone = (TextView) v.findViewById(R.id.phone);
        gemail = (TextView) v.findViewById(R.id.email);
        gagency = (TextView) v.findViewById(R.id.agency);
        Picasso.get().load(userImage).placeholder(R.drawable.profile_image).into(userProfileImage);

        back = v.findViewById(R.id.arrowback1);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DashboardActivity.class);
                startActivity(i);
                getActivity().finish();

            }
        });

        gname.setText(name);
        gphone.setText(number);
        gemail.setText(email);
        gid.setText(guardid);
        gagency.setText(agency);

        return v;

    }
}