package askari.com.guard.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import askari.com.guard.Activities.DashboardActivity;
import askari.com.guard.Activities.IncidentReport;
import askari.com.guard.Activities.LeaveRequest;
import askari.com.guard.Activities.ReceiptLog;
import askari.com.guard.Activities.ddob;
import askari.com.guard.R;


public class MoreFragment extends Fragment {

    TextView leave,incident,occurence;
    RelativeLayout back;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_more, container, false);

        leave = v.findViewById(R.id.leave);
        occurence = v.findViewById(R.id.occurrence);
        incident = v.findViewById(R.id.incident);

        back = v.findViewById(R.id.arrowback3);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), DashboardActivity.class);
                startActivity(i);
                getActivity().finish();

            }
        });

        leave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LeaveRequest.class);
                startActivity(i);
            }
        });

        occurence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectLog(getActivity());
            }
        });
        incident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), IncidentReport.class);
                startActivity(i);
            }
        });
        return v;
    }

    private void selectLog(Context context) {
        final CharSequence[] options = {"Occurrence Log", "Receipt Log", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Select a Log:");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Occurrence Log")) {
                    Intent i = new Intent(getActivity(), ddob.class);
                    startActivity(i);

                } else if (options[item].equals("Receipt Log")) {
                    Intent i = new Intent(getActivity(), ReceiptLog.class);
                    startActivity(i);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
}