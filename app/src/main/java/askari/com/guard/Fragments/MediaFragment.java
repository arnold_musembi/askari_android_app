package askari.com.guard.Fragments;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import askari.com.guard.R;
import askari.com.guard.Utilities.SessionManager;
import askari.com.guard.incidentRetrofit.Retimager;
import askari.com.guard.incidentRetrofit.network.ServiceInterface;
import askari.com.guard.incidentRetrofit.utils.FileUtil;
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class MediaFragment extends Fragment {
    ImageView im1,im2,im3;
   Button sub;
    ImageView selectedImage,test,test1;
    Bitmap bitmap;
    String encodedimage,encodedimage1,encodedimage2;
    CircularProgressButton btnSubmit;
    ServiceInterface serviceInterface;
    List<Uri> files = new ArrayList<>();
    SessionManager session;
    String guard_id, name, longitude, latitude, batlev,androidID;
    private LinearLayout parentLinearLayout;


    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_media, container, false);
        sub = v.findViewById(R.id.incidentsubmit);

        im1 = v.findViewById(R.id.im1);
        im2 = v.findViewById(R.id.im2);
        im3 = v.findViewById(R.id.im3);

        requestPermission();

        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage1();
            }
        });
        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage2();
            }
        });
        im3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImage3();
            }
        });



        return v;
    }

    public void addImage1() {
        selectImage1(getActivity());
    }
    public void addImage2() {
        selectImage2(getActivity());
    }
    public void addImage3() {
        selectImage3(getActivity());
    }

    private void selectImage1(Context context) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Choose a Media");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);//one can be replaced with any action code

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void selectImage2(Context context) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Choose a Media");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 2);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 3);//one can be replaced with any action code

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void selectImage3(Context context) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Choose a Media");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 4);

                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 5);//one can be replaced with any action code

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {

                        bitmap =(Bitmap)data.getExtras().get("data");
                        im1.setImageBitmap(bitmap);
                        encodebitmap(bitmap);
//                        Picasso.get().load(getImageUri(getActivity(),bitmap)).into(im1);

                        String imgPath = FileUtil.getPath(getActivity(),getImageUri(getActivity(),bitmap));

                        files.add(Uri.parse(imgPath));
                        Log.e("image", imgPath);
                    }

                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri img = data.getData();
                        Picasso.get().load(img).into(im1);

                        try{
                            InputStream inputStream = getActivity().getContentResolver().openInputStream(img);
                            bitmap = BitmapFactory.decodeStream(inputStream);
                            encodebitmap(bitmap);

                        }catch (FileNotFoundException e){
                            e.printStackTrace();
                        }

                        String imgPath = FileUtil.getPath(getActivity(), img);

                        files.add(Uri.parse(imgPath));
                        Log.e("image", imgPath);

                    }
                    break;

                case 2:
                    if (resultCode == RESULT_OK && data != null) {

                        bitmap =(Bitmap)data.getExtras().get("data");
                        im2.setImageBitmap(bitmap);
                        encodebitmap1(bitmap);
//                        Picasso.get().load(getImageUri(getActivity(),bitmap)).into(im2);

                        String imgPath = FileUtil.getPath(getActivity(),getImageUri(getActivity(),bitmap));

                        files.add(Uri.parse(imgPath));
                        Log.e("image", imgPath);
                    }
                    break;
                case 3:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri img = data.getData();
                        Picasso.get().load(img).into(im2);

                        try{
                            InputStream inputStream = getActivity().getContentResolver().openInputStream(img);
                            bitmap = BitmapFactory.decodeStream(inputStream);
                            encodebitmap1(bitmap);

                        }catch (FileNotFoundException e){
                            e.printStackTrace();
                        }

                        String imgPath = FileUtil.getPath(getActivity(), img);

                        files.add(Uri.parse(imgPath));
                        Log.e("image", imgPath);

                    }
                    break;

                case 4:
                    if (resultCode == RESULT_OK && data != null) {

                        bitmap =(Bitmap)data.getExtras().get("data");
                        im3.setImageBitmap(bitmap);
                        encodebitmap2(bitmap);
//                        Picasso.get().load(getImageUri(getActivity(),bitmap)).into(im3);

                        String imgPath = FileUtil.getPath(getActivity(),getImageUri(getActivity(),bitmap));

                        files.add(Uri.parse(imgPath));
                        Log.e("image", imgPath);
                    }
                    break;
                case 5:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri img = data.getData();
                        Picasso.get().load(img).into(im3);

                        try{
                            InputStream inputStream = getActivity().getContentResolver().openInputStream(img);
                            bitmap = BitmapFactory.decodeStream(inputStream);
                            encodebitmap2(bitmap);

                        }catch (FileNotFoundException e){
                            e.printStackTrace();
                        }

                        String imgPath = FileUtil.getPath(getActivity(), img);

                        files.add(Uri.parse(imgPath));
                        Log.e("image", imgPath);

                    }
                    break;
            }
        }
    }
    private void encodebitmap(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);

        byte[] byteofimages=byteArrayOutputStream.toByteArray();
        encodedimage= Base64.encodeToString(byteofimages, Base64.DEFAULT);
    }
    private void encodebitmap1(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);

        byte[] byteofimages=byteArrayOutputStream.toByteArray();
       encodedimage1 = Base64.encodeToString(byteofimages, Base64.DEFAULT);
    }
    private void encodebitmap2(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);

        byte[] byteofimages=byteArrayOutputStream.toByteArray();
        encodedimage2= Base64.encodeToString(byteofimages, Base64.DEFAULT);
    }
    //===== bitmap to Uri
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "askari", null);
        Log.d("image uri",path);

        byte[] byteofimages =bytes.toByteArray();
        encodedimage= Base64.encodeToString(byteofimages, Base64.DEFAULT);
        encodedimage1= Base64.encodeToString(byteofimages, Base64.DEFAULT);
        encodedimage2= Base64.encodeToString(byteofimages, Base64.DEFAULT);
        return Uri.parse(path);
//        return encodedimage;
    }






    // this is all you need to grant your application external storage permision
    private void requestPermission(){
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){
            case REQUEST_CODE_ASK_PERMISSIONS:
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
//                    retima();
                }
                else {
                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }


}