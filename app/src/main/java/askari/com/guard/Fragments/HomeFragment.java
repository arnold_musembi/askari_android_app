package askari.com.guard.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;


import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import askari.com.guard.Activities.AssignInstruction;
import askari.com.guard.incidentRetrofit.Retimager;
import io.fabric.sdk.android.Fabric;
import askari.com.guard.Activities.AccessCode;
import askari.com.guard.Activities.AttendenceActivity;
import askari.com.guard.Activities.DashboardActivity;
import askari.com.guard.Activities.EndShift;
import askari.com.guard.Activities.LoginActivity;
import askari.com.guard.Activities.MissedActivity;
import askari.com.guard.Activities.ScanCheckpoint;
import askari.com.guard.Adapters.TotalRoundAdapter;
import askari.com.guard.Models.RoundModel;
import askari.com.guard.R;
import askari.com.guard.Utilities.Constant;
import askari.com.guard.Utilities.SessionManager;

import android.provider.Settings;
import android.provider.Settings.System;


public class HomeFragment extends Fragment implements LocationListener {

    TextView strt, nameC, descript, intime, duration, total, complete, check, gname, getim, endshift;
    FloatingActionButton flash;
    RelativeLayout rounds,ai;
    Dialog dialog;
    private Camera camera;
    boolean showingFirst = true;
    ImageView logoutbtn, cancel;
    SessionManager session;
    String guard_id, name, longitude, latitude, batlev,androidID;
    RelativeLayout viewattend;
    String endtime;
    ShimmerFrameLayout mShimmerViewContainer;
    RelativeLayout companyrr, missedrr;

    private LocationManager locationManager;


    String i = "1";

    RecyclerView recyclerView;
    TotalRoundAdapter rAdapter;
    StringRequest stringRequest, stringRequest1;
    RequestQueue mRequestQueue, mRequestQueue1;
    private List<RoundModel> list = new ArrayList<>();
    public static final String TAG = "STag";

    private CameraManager mCameraManager;
    private String mCameraId;

    TextView battery,textView;





    private BroadcastReceiver batterylevelReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
         int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
//            battery.setText(String.valueOf(level) + "%");
             batlev = String.valueOf(level);
        }
    };


    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        Fabric.with(getActivity(), new Crashlytics());
        //Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

//        battery = v.findViewById(R.id.battext);
        getActivity().registerReceiver(this.batterylevelReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        session = new SessionManager(getActivity());

        strt = v.findViewById(R.id.startScan);
        endshift = v.findViewById(R.id.endshift);
        nameC = v.findViewById(R.id.cname);
        gname = v.findViewById(R.id.textname);
        descript = v.findViewById(R.id.description);
        duration = v.findViewById(R.id.shiftD);
        intime = v.findViewById(R.id.intime);
        viewattend = v.findViewById(R.id.viewattendence);
        total = v.findViewById(R.id.tround);
        complete = v.findViewById(R.id.cround);
        check = v.findViewById(R.id.check);
        rounds = v.findViewById(R.id.r2);
        mShimmerViewContainer = v.findViewById(R.id.shimmer_view_container);
        companyrr = v.findViewById(R.id.companyinfo);
        missedrr = v.findViewById(R.id.missR);

        ai = v.findViewById(R.id.ai);

        logoutbtn = v.findViewById(R.id.logout);

        HashMap<String, String> users = session.getUserDetails();
        guard_id = users.get(session.KEY_ID);
        name = users.get(session.KEY_NAME);

        gname.setText(name);
        textView = v.findViewById(R.id.text);


        androidID = System.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
//        textView.setText(androidID);





        locationManager = (LocationManager)  getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        final Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        onLocationChanged(location);



        ai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), AssignInstruction.class);
                startActivity(i);
            }
        });

//        getim.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getContext(), Retimager.class);
//                startActivity(i);
//            }
//        });

        //session.checkLogin();

        strt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ScanCheckpoint.class);
                startActivity(i);
//                phoneInfo();
            }
        });

        endshift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Do you want to end today's Shift?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Intent i = new Intent(getContext(), EndShift.class);
                                startActivity(i);
                                getActivity().finish();

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        rounds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog = new Dialog(getActivity(), R.style.Theme_Dialog);
                dialog.setTitle("Rounds");
                dialog.setContentView(R.layout.dialog);
                dialog.setCanceledOnTouchOutside(false);

                recyclerView = (RecyclerView) dialog.findViewById(R.id.RoundRecycler);
                recyclerView.setHasFixedSize(false);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);

                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());

                RoundInfo();

                cancel = dialog.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }

        });

        logoutbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Logout();

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });


        final boolean isFlashAvailable = getContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCameraManager = (CameraManager) getContext().getSystemService(Context.CAMERA_SERVICE);
            try {
                mCameraId = mCameraManager.getCameraIdList()[0];
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
        if (!isFlashAvailable) {
            showNoFlashError();
        }



        flash = v.findViewById(R.id.fab);
        flash.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               /* if (showingFirst == true) {

                    showingFirst = false;
                    camera = Camera.open();
                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(parameters);
                    camera.startPreview();

                } else {

                    showingFirst = true;

                    camera = Camera.open();
                    Camera.Parameters parameters = camera.getParameters();
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(parameters);
                    camera.stopPreview();
                    camera.release();
                }*/

                if(i.equals("1")){
                    i="2";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        switchFlashLight(true);
                    }
                }else{
                    i="1";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        switchFlashLight(false);
                    }
                }

            }
        });

        viewattend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AttendenceActivity.class);
                startActivity(i);
            }
        });

        missedrr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MissedActivity.class);
                startActivity(i);
            }
        });

        Aboutus();
        return v;
    }



    @Override
    public void onStart() {
        super.onStart();
        ((DashboardActivity) getActivity()).startLocationService();
    }


    public void showNoFlashError() {
        AlertDialog alert = new AlertDialog.Builder(getContext())
                .create();
        alert.setTitle("Oops!");
        alert.setMessage("Flash not available in this device...");
        alert.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        });
        alert.show();

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void switchFlashLight(boolean status) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCameraManager.setTorchMode(mCameraId, status);
            }
        } catch (@SuppressLint({"NewApi", "LocalSuppress"}) CameraAccessException e) {
            e.printStackTrace();
        }
    }

    public void RoundInfo() {

        final ProgressDialog showMe = new ProgressDialog(getActivity(),AlertDialog.THEME_HOLO_LIGHT);
        showMe.setMessage("Please wait");
        showMe.setCancelable(true);
        showMe.setCanceledOnTouchOutside(false);
        showMe.show();

        mRequestQueue = Volley.newRequestQueue(getActivity());

        stringRequest = new StringRequest(Request.Method.POST, Constant.ROUND_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showMe.dismiss();
                        list.clear();
                        JSONObject j = null;
                        try {
                            j = new JSONObject(response);

                            String status = j.getString("status");
                            String msg = j.getString("msg");
                            if(status.equals("500")) {
                                session.logoutUser();
                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                startActivity(i);
                                getActivity().finish();
                                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            }
                            if (status.equals("200")) {
                                JSONArray applist = j.getJSONArray("data");
                                if (applist != null && applist.length() > 0) {
                                    for (int i = 0; i < applist.length(); i++) {

                                        RoundModel model = new RoundModel();
                                        JSONObject getOne = applist.getJSONObject(i);

                                        model.setRname(getOne.getString("round_name"));
                                        model.setTime(getOne.getString("duration"));

                                        list.add(model);
                                        rAdapter = new TotalRoundAdapter(getActivity(), list);
                                        recyclerView.setAdapter(rAdapter);

                                    }
                                } else {
                                    showMe.dismiss();
                                    Toast toast = Toast.makeText(getActivity(), "" + j.getString("msg"), Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();

                                }
                            } else {

                                showMe.dismiss();
                                Toast toast = Toast.makeText(getActivity(), "" + j.getString("msg"), Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                //  nodata.setVisibility(View.VISIBLE);
                                //  recyclerView.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            showMe.dismiss();

                            Log.e("TAG", "Something Went Wrong");
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showMe.dismiss();

                        NetworkDialog();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("guard_id", guard_id);
                return headers;
            }

        };
        stringRequest.setTag(TAG);
        mRequestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }


    public void Aboutus() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.ABOUT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        //showMe.dismiss();

                        JSONObject j = null;
                        try {
                            j = new JSONObject(ServerResponse);

                            String status = j.getString("status");
                            if (status.equals("200")) {


                                String title = j.getString("title");
                                String descripts = j.getString("description");
                                String shiftTime = j.getString("shift");
                                String intimee = j.getString("intime");
                                String tround = j.getString("total_rounds");
                                String cround = j.getString("completed_rounds");
                                String mround = j.getString("missed_checkpoints");
                                endtime = j.getString("end");


                                nameC.setText(title);
                                descript.setText(descripts);
                                duration.setText(shiftTime);
                                intime.setText("IN time: " + intimee);
                                total.setText(tround);
                                complete.setText(cround);
                                check.setText(mround);
                                //  showMe.dismiss();

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                companyrr.setVisibility(View.VISIBLE);

                            }else if(status.equals("500")) {
                                String m = j.getString("msg");
                                session.logoutUser();
                                getActivity().finish();
                                Intent i = new Intent(getActivity(), AccessCode.class);
                                startActivity(i);
                                Toast.makeText(getActivity(), m, Toast.LENGTH_SHORT).show();

                            }
                            else {
                                String failed = j.getString("msg");
                                // showMe.dismiss();
                                Toast.makeText(getActivity(), failed, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // showMe.dismiss();
                        if (getActivity() != null) {

                            NetworkDialog();
                        }

                        //Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                params.put("guard_id", guard_id);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }





    public LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if (locationResult != null && locationResult.getLastLocation() != null) {
                 latitude = String.valueOf(locationResult.getLastLocation().getLatitude());
                 longitude = String.valueOf(locationResult.getLastLocation().getLongitude());
//                Log.d("LOCATION_UPDATE", latitude + "," + longitude);
//                getloc.setText("longitude:" +longitude + "\n" + "latitude:" + latitude);
//                String mylat =  String.valueOf(latitude);
//                String mylong =  String.valueOf(longitude);

            }
        }
    };

    @SuppressLint("MissingPermission")
    @Override
    public void onLocationChanged(Location location) {
//        double longitud = location.getLongitude();
//        double latitud = location.getLatitude();
//        getloc.setText("longitude:" +longitud + "\n" + "latitude:" + latitud);

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(4000);
        locationRequest.setFastestInterval(2000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        LocationServices.getFusedLocationProviderClient(getActivity())
                .requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
    }

    public void phoneInfo() {


        final String guard = guard_id;
        final String mylong = longitude;
        final String lat = latitude;
        final String batterylev = batlev;
        final String imei = androidID;


                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.PHONE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        //showMe.dismiss();
                        JSONObject j = null;
                        try {
                            j = new JSONObject(ServerResponse);

                            String status = j.getString("status");
                            if (status.equals("200")) {


                                String title = j.getString("msg");
//                                String descripts = j.getString("description");
//                                String shiftTime = j.getString("shift");
//                                String intimee = j.getString("intime");
//                                String tround = j.getString("total_rounds");
//                                String cround = j.getString("completed_rounds");
//                                String mround = j.getString("missed_checkpoints");
//                                endtime = j.getString("end");

                                Toast.makeText(getActivity(), title, Toast.LENGTH_LONG).show();
                                battery.setText(title);
//                                descript.setText(descripts);
//                                duration.setText(shiftTime);
//                                intime.setText("IN time: " + intimee);
//                                total.setText(tround);
//                                complete.setText(cround);
//                                check.setText(mround);
//                                //  showMe.dismiss();
//
//                                mShimmerViewContainer.stopShimmerAnimation();
//                                mShimmerViewContainer.setVisibility(View.GONE);
//                                companyrr.setVisibility(View.VISIBLE);

                            }else if(status.equals("500")) {
                                String m = j.getString("msg");
//                                session.logoutUser();
//                                getActivity().finish();
//                                Intent i = new Intent(getActivity(), LoginActivity.class);
//                                startActivity(i);
                                Toast.makeText(getActivity(), m, Toast.LENGTH_SHORT).show();

                            }
                            else {
                                String failed = j.getString("msg");
                                // showMe.dismiss();
                                Toast.makeText(getActivity(), failed, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("response",ServerResponse + latitude +","+ longitude +","+ guard_id + "," + androidID +","+ batlev);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // showMe.dismiss();
                        if (getActivity() != null) {
                            Log.d("volleyError",""+volleyError);
                            NetworkDialog();
                        }

                        //Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                params.put("imei",imei);
                params.put("longitude",mylong);
                params.put("latitude",lat);
                params.put("bat",batterylev);
                params.put("guard_id", guard);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }




    public void Logout() {
        final ProgressDialog showMe = new ProgressDialog(getActivity(),AlertDialog.THEME_HOLO_LIGHT);
        showMe.setMessage("Please wait");
        showMe.setCancelable(true);
        showMe.setCanceledOnTouchOutside(false);
        showMe.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.LOGOUT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        showMe.dismiss();

                        JSONObject j = null;
                        try {
                            j = new JSONObject(ServerResponse);

                            String status = j.getString("status");
                            if (status.equals("200")) {

                                session.logoutUser();
                                getActivity().finish();
                                Toast.makeText(getActivity(), "Successfully Logout", Toast.LENGTH_SHORT).show();
                                showMe.dismiss();

                            } else {
                                showMe.dismiss();
                                String msg = j.getString("msg");
                                Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            showMe.dismiss();
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        showMe.dismiss();
                        NetworkDialog1();
                        // Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("guard_id", guard_id);

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

    private void NetworkDialog() {
        final Dialog dialogs = new Dialog(getActivity());
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setContentView(R.layout.networkdialog);
        dialogs.setCanceledOnTouchOutside(false);
        Button done = (Button) dialogs.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();
                Aboutus();

            }
        });
        dialogs.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    private void NetworkDialog1() {
        final Dialog dialogs = new Dialog(getActivity());
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setContentView(R.layout.networkdialog);
        dialogs.setCanceledOnTouchOutside(false);
        Button done = (Button) dialogs.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();
                Logout();

            }
        });
        dialogs.show();
    }



}
