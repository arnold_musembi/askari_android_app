package askari.com.guard.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import askari.com.guard.R;
import askari.com.guard.Utilities.Constant;
import askari.com.guard.Utilities.SessionManager;

public class ddob extends AppCompatActivity {
    EditText occurrence,remarks;
    Button dobbtn;
    ImageView back;

    SessionManager session;
    String guard_id, name, longitude, latitude, batlev,androidID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ddob);

        session = new SessionManager(ddob.this);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> users = session.getUserDetails();
        guard_id = users.get(session.KEY_ID);
        name = users.get(session.KEY_NAME);

        back = findViewById(R.id.arrowback5);
        occurrence = findViewById(R.id.occurrence);
        remarks = findViewById(R.id.remarks);
        dobbtn = findViewById(R.id.ddobbtn);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ddob.this,DashboardActivity.class);
                startActivity(i);
                ddob.this.finish();

            }
        });

        dobbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                report_occurence();
            }
        });

    }

    private void report_occurence() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.DDOB,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        //showMe.dismiss();
                        JSONObject j = null;
                        try {
                            j = new JSONObject(ServerResponse);

                            String status = j.getString("status");
                            if (status.equals("200")) {


                                String title = j.getString("msg");
//                                String descripts = j.getString("description");
//                                String shiftTime = j.getString("shift");
//                                String intimee = j.getString("intime");
//                                String tround = j.getString("total_rounds");
//                                String cround = j.getString("completed_rounds");
//                                String mround = j.getString("missed_checkpoints");
//                                endtime = j.getString("end");

                                Toast.makeText(ddob.this,title, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(ddob.this, DashboardActivity.class);
                                startActivity(intent);
                                ddob.this.finish();
//                                descript.setText(descripts);
//                                duration.setText(shiftTime);
//                                intime.setText("IN time: " + intimee);
//                                total.setText(tround);
//                                complete.setText(cround);
//                                check.setText(mround);
//                                //  showMe.dismiss();
//
//                                mShimmerViewContainer.stopShimmerAnimation();
//                                mShimmerViewContainer.setVisibility(View.GONE);
//                                companyrr.setVisibility(View.VISIBLE);

                            }else if(status.equals("500")) {
                                String m = j.getString("msg");
//                                session.logoutUser();
//                                getActivity().finish();
//                                Intent i = new Intent(getActivity(), LoginActivity.class);
//                                startActivity(i);
                                Toast.makeText(ddob.this, m, Toast.LENGTH_SHORT).show();

                            }
                            else {
                                String failed = j.getString("msg");
                                // showMe.dismiss();
                                Toast.makeText(ddob.this, failed, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // showMe.dismiss();
                        if (ddob.this!= null) {
                            Log.d("volleyError",""+volleyError);
                            Toast.makeText(ddob.this, "occurrence was not submitted!!", Toast.LENGTH_SHORT).show();
                        }

                        //Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                params.put("occurrence",occurrence.getText().toString());
                params.put("remarks",remarks.getText().toString());
                params.put("guard_id",guard_id);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(ddob.this);
        requestQueue.add(stringRequest);
    }

    private void NetworkDialog1() {
        final Dialog dialogs = new Dialog(ddob.this);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setContentView(R.layout.networkdialog);
        dialogs.setCanceledOnTouchOutside(false);
        Button done = (Button) dialogs.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();


            }
        });
        dialogs.show();
    }
}