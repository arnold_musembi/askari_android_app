package askari.com.guard.Activities;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import askari.com.guard.Fragments.MediaFragment;
import askari.com.guard.Fragments.TitleFragment;
import askari.com.guard.Fragments.WitnessaFragment;
import askari.com.guard.Fragments.WitnessbFragment;
import askari.com.guard.R;
import askari.com.guard.Utilities.Constant;
import askari.com.guard.Utilities.SessionManager;

public class IncidentReport extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    Button sub;
    EditText incname,pob,incdesc,actiontaken,sia;
    String guard_id, name;
    SessionManager session;
    private TitleFragment titleFragment;
    private WitnessaFragment witnessaFragment;
    private WitnessbFragment witnessbFragment;
    private MediaFragment mediaFragment;
    RelativeLayout back;

    String encodedimage,encodedimage1,encodedimage2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_report);


        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tab_layout);
        back = findViewById(R.id.arrowback1);
        sub = findViewById(R.id.incidentsubmit);

        session = new SessionManager(IncidentReport.this);
        HashMap<String, String> users = session.getUserDetails();
        guard_id = users.get(session.KEY_ID);
        name = users.get(session.KEY_NAME);

        incname = findViewById(R.id.name);
        pob = findViewById(R.id.pob);
        incdesc = findViewById(R.id.incdesc);
        actiontaken = findViewById(R.id.actiontaken);
        sia = findViewById(R.id.sia);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(IncidentReport.this,DashboardActivity.class);
                startActivity(i);
                finish();

            }
        });

//        sub.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Incident_submit();
//            }
//        });

        titleFragment = new TitleFragment();
        mediaFragment = new MediaFragment();

        tabLayout.setupWithViewPager(viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), 0);
        viewPagerAdapter.addFragment(titleFragment, "Title");
        viewPagerAdapter.addFragment(mediaFragment, "Images");
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_baseline_title_24);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_baseline_perm_media_24);


    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments = new ArrayList<>();
        private List<String> fragmentTitle = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        public void addFragment(Fragment fragment, String title) {
            fragments.add(fragment);
            fragmentTitle.add(title);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position);
        }
    }


//    SUBMIT INCIDENT REPORT TO SERVER
    private void Incident_submit() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.RET,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        //showMe.dismiss();
                        JSONObject j = null;
                        try {
                            j = new JSONObject(ServerResponse);

                            String status = j.getString("status");
                            if (status.equals("200")) {


                                String title = j.getString("msg");
//                                String descripts = j.getString("description");
//                                String shiftTime = j.getString("shift");
//                                String intimee = j.getString("intime");
//                                String tround = j.getString("total_rounds");
//                                String cround = j.getString("completed_rounds");
//                                String mround = j.getString("missed_checkpoints");
//                                endtime = j.getString("end");

                                Toast.makeText(IncidentReport.this,title, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(IncidentReport.this, DashboardActivity.class);
                                startActivity(intent);
                                IncidentReport.this.finish();
//                                descript.setText(descripts);
//                                duration.setText(shiftTime);
//                                intime.setText("IN time: " + intimee);
//                                total.setText(tround);
//                                complete.setText(cround);
//                                check.setText(mround);
//                                //  showMe.dismiss();
//
//                                mShimmerViewContainer.stopShimmerAnimation();
//                                mShimmerViewContainer.setVisibility(View.GONE);
//                                companyrr.setVisibility(View.VISIBLE);

                            }else if(status.equals("500")) {
                                String m = j.getString("msg");
//                                session.logoutUser();
//                                getActivity().finish();
//                                Intent i = new Intent(getActivity(), LoginActivity.class);
//                                startActivity(i);
                                Toast.makeText(IncidentReport.this, m, Toast.LENGTH_SHORT).show();

                            }
                            else {
                                String failed = j.getString("msg");
                                // showMe.dismiss();
                                Toast.makeText(IncidentReport.this, failed, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // showMe.dismiss();
                        if (IncidentReport.this!= null) {
                            Log.d("volleyError",""+volleyError);
                            Toast.makeText(IncidentReport.this, "Wrong password", Toast.LENGTH_SHORT).show();
                        }

                        //Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                params.put("inctitle",incname.getText().toString());
                params.put("pob",pob.getText().toString());
                params.put("incdesc",incdesc.getText().toString());
                params.put("acttaken",actiontaken.getText().toString());
                params.put("sia",sia.getText().toString());
                params.put("guard_id",guard_id);

                params.put("file", encodedimage);
                params.put("file1", encodedimage1);
                params.put("file2", encodedimage2);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(IncidentReport.this);
        requestQueue.add(stringRequest);

    }

}