package askari.com.guard.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import askari.com.guard.R;
import askari.com.guard.Utilities.Constant;
import askari.com.guard.Utilities.SessionManager;

public class LeaveRequest extends AppCompatActivity {

    EditText todate,fromdate,leavereason;
    RelativeLayout back;
    Button leavebtn,status;

    SessionManager session;
    String guard_id, name, longitude, latitude, batlev,androidID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_request);

        session = new SessionManager(LeaveRequest.this);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> users = session.getUserDetails();
        guard_id = users.get(session.KEY_ID);
        name = users.get(session.KEY_NAME);

        back = findViewById(R.id.arrowback3);
        status= findViewById(R.id.status);
        fromdate = findViewById(R.id.fromdate);
        todate = findViewById(R.id.todate);
        leavebtn = findViewById(R.id.leavebtn);
        leavereason = findViewById(R.id.leavereason);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(LeaveRequest.this,DashboardActivity.class);
                startActivity(i);
                LeaveRequest.this.finish();

            }
        });

        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LeaveRequest.this,leaveStatus.class);
                startActivity(i);
                LeaveRequest.this.finish();
            }
        });

        fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                final int[] mYear = {mcurrentDate.get(Calendar.YEAR)};
                final int[] mMonth = {mcurrentDate.get(Calendar.MONTH)};
                final int[] mDay = {mcurrentDate.get(Calendar.DAY_OF_MONTH)};

                DatePickerDialog mDatePicker = new DatePickerDialog(LeaveRequest.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        mYear[0] = selectedyear;
                        mMonth[0] = selectedmonth;
                        mDay[0] = selectedday;
                        fromdate.setText(new StringBuilder().append(mDay[0]).append("/").append(mMonth[0] + 1).append("/").append(mYear[0]).append("")); }
                }, mYear[0], mMonth[0], mDay[0]);
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();
//                        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());

            }
        });

        todate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                final int[] mYear = {mcurrentDate.get(Calendar.YEAR)};
                final int[] mMonth = {mcurrentDate.get(Calendar.MONTH)};
                final int[] mDay = {mcurrentDate.get(Calendar.DAY_OF_MONTH)};

                DatePickerDialog mDatePicker = new DatePickerDialog(LeaveRequest.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        mYear[0] = selectedyear;
                        mMonth[0] = selectedmonth;
                        mDay[0] = selectedday;
                        todate.setText(new StringBuilder().append(mDay[0]).append("/").append(mMonth[0] + 1).append("/").append(mYear[0]).append("")); }

                }, mYear[0], mMonth[0], mDay[0]);
                mDatePicker.setTitle("Select Date");
                mDatePicker.show();
//                        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());

            }
        });


        leavebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Leave_submit();
            }
        });
    }

    private void Leave_submit() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.LEAVE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        //showMe.dismiss();
                        JSONObject j = null;
                        try {
                            j = new JSONObject(ServerResponse);

                            String status = j.getString("status");
                            if (status.equals("200")) {


                                String title = j.getString("msg");
//                                String descripts = j.getString("description");
//                                String shiftTime = j.getString("shift");
//                                String intimee = j.getString("intime");
//                                String tround = j.getString("total_rounds");
//                                String cround = j.getString("completed_rounds");
//                                String mround = j.getString("missed_checkpoints");
//                                endtime = j.getString("end");

                                Toast.makeText(LeaveRequest.this,title, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(LeaveRequest.this, LoginActivity.class);
                                startActivity(intent);
                                LeaveRequest.this.finish();
//                                descript.setText(descripts);
//                                duration.setText(shiftTime);
//                                intime.setText("IN time: " + intimee);
//                                total.setText(tround);
//                                complete.setText(cround);
//                                check.setText(mround);
//                                //  showMe.dismiss();
//
//                                mShimmerViewContainer.stopShimmerAnimation();
//                                mShimmerViewContainer.setVisibility(View.GONE);
//                                companyrr.setVisibility(View.VISIBLE);

                            }else if(status.equals("500")) {
                                String m = j.getString("msg");
//                                session.logoutUser();
//                                getActivity().finish();
//                                Intent i = new Intent(getActivity(), LoginActivity.class);
//                                startActivity(i);
                                Toast.makeText(LeaveRequest.this, m, Toast.LENGTH_SHORT).show();

                            }
                            else {
                                String failed = j.getString("msg");
                                // showMe.dismiss();
                                Toast.makeText(LeaveRequest.this, failed, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // showMe.dismiss();
                        if (LeaveRequest.this!= null) {
                            Log.d("volleyError",""+volleyError);
                            Toast.makeText(LeaveRequest.this, "Wrong password", Toast.LENGTH_SHORT).show();
                        }

                        //Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                params.put("startdate",fromdate.getText().toString());
                params.put("enddate",todate.getText().toString());
                params.put("reason",leavereason.getText().toString());
                params.put("guard_id",guard_id);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(LeaveRequest.this);
        requestQueue.add(stringRequest);

    }

    private void NetworkDialog1() {
        final Dialog dialogs = new Dialog(LeaveRequest.this);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setContentView(R.layout.networkdialog);
        dialogs.setCanceledOnTouchOutside(false);
        Button done = (Button) dialogs.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();


            }
        });
        dialogs.show();
    }
}