package askari.com.guard.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.github.barteksc.pdfviewer.PDFView;

import askari.com.guard.R;

public class AssignInstruction extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_instruction);

        PDFView pdfView = findViewById(R.id.pdfView);

        pdfView.fromAsset("ai.pdf").load();

    }
}