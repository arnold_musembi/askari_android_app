package askari.com.guard.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import askari.com.guard.R;
import askari.com.guard.Utilities.Constant;
import askari.com.guard.Utilities.SessionManager;

public class AccessCode extends AppCompatActivity {

    EditText accesscod;
    Button accessbtn;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_code);

        accesscod = findViewById(R.id.accesscode);
        accessbtn = findViewById(R.id.accessBtn);
        session = new SessionManager(getApplicationContext());

        accessbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Access();
            }
        });

        if (session.isLoggedIn() == true) {

            Intent intent = new Intent(AccessCode.this, DashboardActivity.class);
            startActivity(intent);
            finish();

        }


    }

    public void Access() {



        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.ACCESS_CODE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        //showMe.dismiss();
                        JSONObject j = null;
                        try {
                            j = new JSONObject(ServerResponse);

                            String status = j.getString("status");
                            if (status.equals("200")) {


                                String title = j.getString("msg");
//                                String descripts = j.getString("description");
//                                String shiftTime = j.getString("shift");
//                                String intimee = j.getString("intime");
//                                String tround = j.getString("total_rounds");
//                                String cround = j.getString("completed_rounds");
//                                String mround = j.getString("missed_checkpoints");
//                                endtime = j.getString("end");

                                Toast.makeText(AccessCode.this,title, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(AccessCode.this, LoginActivity.class);
                                startActivity(intent);
                                AccessCode.this.finish();
//                                descript.setText(descripts);
//                                duration.setText(shiftTime);
//                                intime.setText("IN time: " + intimee);
//                                total.setText(tround);
//                                complete.setText(cround);
//                                check.setText(mround);
//                                //  showMe.dismiss();
//
//                                mShimmerViewContainer.stopShimmerAnimation();
//                                mShimmerViewContainer.setVisibility(View.GONE);
//                                companyrr.setVisibility(View.VISIBLE);

                            }else if(status.equals("500")) {
                                String m = j.getString("msg");
//                                session.logoutUser();
//                                getActivity().finish();
//                                Intent i = new Intent(getActivity(), LoginActivity.class);
//                                startActivity(i);
                                Toast.makeText(AccessCode.this, m, Toast.LENGTH_SHORT).show();

                            }
                            else {
                                String failed = j.getString("msg");
                                // showMe.dismiss();
                                Toast.makeText(AccessCode.this, failed, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // showMe.dismiss();
                        if (AccessCode.this!= null) {
                            Log.d("volleyError",""+volleyError);
                            Toast.makeText(AccessCode.this, "Wrong password", Toast.LENGTH_SHORT).show();
                        }

                        //Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                params.put("accesscode",accesscod.getText().toString());

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(AccessCode.this);
        requestQueue.add(stringRequest);
    }

//    public void CList() {
//
//        final ProgressDialog showMe = new ProgressDialog(AccessCode.this, AlertDialog.THEME_HOLO_LIGHT);
//        showMe.setMessage("Please wait");
//        showMe.setCancelable(true);
//        showMe.setCanceledOnTouchOutside(false);
//        showMe.show();
//
//        mRequestQueue = Volley.newRequestQueue(AccessCode.this);
//
//        stringRequest = new StringRequest(Request.Method.POST, Constant.SCAN_URL,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        showMe.dismiss();
//                        list.clear();
//                        JSONObject j = null;
//                        try {
//                            j = new JSONObject(response);
//
//                            String status = j.getString("status");
//                            String msg = j.getString("msg");
//
//                            if(status.equals("500")) {
//                                session.logoutUser();
//                                Intent i = new Intent(ScanCheckpoint.this, LoginActivity.class);
//                                startActivity(i);
//                                finish();
//                                Toast.makeText(ScanCheckpoint.this, msg, Toast.LENGTH_SHORT).show();
//                            }
//                            else if (status.equals("200")) {
//                                JSONArray applist = j.getJSONArray("data");
//                                if (applist != null && applist.length() > 0) {
//                                    for (int i = 0; i < applist.length(); i++) {
//
//                                        CheckPointModel model = new CheckPointModel();
//                                        JSONObject getOne = applist.getJSONObject(i);
//
//                                        model.setId(getOne.getString("id"));
//                                        model.setDate(getOne.getString("date"));
//                                        model.setTime(getOne.getString("time"));
//                                        model.setCheckno(getOne.getString("name"));
//                                        model.setLocation(getOne.getString("location"));
//
//                                        list.add(model);
//                                        rAdapter = new RoundAdapter(getApplicationContext(), list);
//                                        recyclerView.setAdapter(rAdapter);
//                                        recyclerView.setVisibility(View.VISIBLE);
//                                        rr.setVisibility(View.GONE);
//                                        if (!msg.equals("Success")) {
//
//                                            Toast.makeText(ScanCheckpoint.this, msg, Toast.LENGTH_SHORT).show();
//                                        }
//                                    }
//                                } else {
//                                    showMe.dismiss();
//                                    recyclerView.setVisibility(View.GONE);
//                                    rr.setVisibility(View.VISIBLE);
//                                    Toast.makeText(ScanCheckpoint.this, msg, Toast.LENGTH_SHORT).show();
//                                }
//                            } else {
//                                showMe.dismiss();
//                                recyclerView.setVisibility(View.GONE);
//                                rr.setVisibility(View.VISIBLE);
//                                Toast.makeText(ScanCheckpoint.this, msg, Toast.LENGTH_SHORT).show();
//                                //  nodata.setVisibility(View.VISIBLE);
//                                //  recyclerView.setVisibility(View.GONE);
//                            }
//                        } catch (JSONException e) {
//                            showMe.dismiss();
//                            recyclerView.setVisibility(View.GONE);
//                            rr.setVisibility(View.VISIBLE);
//                            Log.e("TAG", "Something Went Wrong");
//                        }
//                    }
//
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        showMe.dismiss();
//                        NetworkDialog();
//                    }
//                }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> headers = new HashMap<String, String>();
//                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
//                return headers;
//            }
//
//            @Override
//            public Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> headers = new HashMap<String, String>();
//
//                if (code == null) {
//
//                    headers.put("qrcode", "0");
//
//                } else {
//
//                    headers.put("qrcode", code);
//                }
//                headers.put("guard_id", id);
//                return headers;
//            }
//
//        };
//        stringRequest.setTag(TAG);
//        mRequestQueue.add(stringRequest);
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//    }

}