package askari.com.guard.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import askari.com.guard.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_homemain);
    }
}