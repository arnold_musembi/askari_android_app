package askari.com.guard.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import askari.com.guard.R;
import askari.com.guard.Utilities.Constant;
import askari.com.guard.Utilities.SessionManager;

public class EndShift extends AppCompatActivity {


    EditText accesscod;
    Button accessbtn;
    SessionManager session;
    String guard_id, name, longitude, latitude, batlev,androidID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_shift);

        session = new SessionManager(EndShift.this);

        accesscod = findViewById(R.id.accesscode);
        accessbtn = findViewById(R.id.accessBtn);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> users = session.getUserDetails();
        guard_id = users.get(session.KEY_ID);
        name = users.get(session.KEY_NAME);

        accessbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Access();
            }
        });
//
//        if (session.isLoggedIn() == true) {
//
//
//        }


    }

    public void Access() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.BOOK_OFF1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        //showMe.dismiss();
                        JSONObject j = null;
                        try {
                            j = new JSONObject(ServerResponse);

                            String status = j.getString("status");
                            if (status.equals("200")) {


                                String title = j.getString("msg");
//                                String descripts = j.getString("description");
//                                String shiftTime = j.getString("shift");
//                                String intimee = j.getString("intime");
//                                String tround = j.getString("total_rounds");
//                                String cround = j.getString("completed_rounds");
//                                String mround = j.getString("missed_checkpoints");
//                                endtime = j.getString("end");

                                Toast.makeText(EndShift.this,title, Toast.LENGTH_LONG).show();

                                Logout();
//                                descript.setText(descripts);
//                                duration.setText(shiftTime);
//                                intime.setText("IN time: " + intimee);
//                                total.setText(tround);
//                                complete.setText(cround);
//                                check.setText(mround);
//                                //  showMe.dismiss();
//
//                                mShimmerViewContainer.stopShimmerAnimation();
//                                mShimmerViewContainer.setVisibility(View.GONE);
//                                companyrr.setVisibility(View.VISIBLE);

                            }else if(status.equals("500")) {
                                String m = j.getString("msg");
//                                session.logoutUser();
//                                getActivity().finish();
//                                Intent i = new Intent(getActivity(), LoginActivity.class);
//                                startActivity(i);
                                Toast.makeText(EndShift.this, m, Toast.LENGTH_SHORT).show();

                            }
                            else {
                                String failed = "Failed to verify the Book-Off OTP!!";
                                // showMe.dismiss();
                                Toast.makeText(EndShift.this, failed, Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // showMe.dismiss();
                        if (EndShift.this!= null) {
                            Log.d("volleyError",""+volleyError);

                            NetworkDialog1();
                        }

                        //Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();
                params.put("accesscode",accesscod.getText().toString());
                params.put("gid",guard_id);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(EndShift.this);
        requestQueue.add(stringRequest);
    }

    public void Logout() {
        final ProgressDialog showMe = new ProgressDialog(EndShift.this, AlertDialog.THEME_HOLO_LIGHT);
        showMe.setMessage("Please wait");
        showMe.setCancelable(true);
        showMe.setCanceledOnTouchOutside(false);
        showMe.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.LOGOUT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        showMe.dismiss();

                        JSONObject j = null;
                        try {
                            j = new JSONObject(ServerResponse);

                            String status = j.getString("status");
                            if (status.equals("200")) {

                                session.logoutUser();
                                EndShift.this.finish();
                                Toast.makeText(EndShift.this, "Successfully Logout", Toast.LENGTH_SHORT).show();
                                showMe.dismiss();

                            } else {
                                showMe.dismiss();
                                String msg = j.getString("msg");
                                Toast.makeText(EndShift.this, msg, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            showMe.dismiss();
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        showMe.dismiss();
                        NetworkDialog1();
                        // Toast.makeText(getActivity(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("apikey", "d29985af97d29a80e40cd81016d939af");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("guard_id", guard_id);

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(EndShift.this);
        requestQueue.add(stringRequest);
    }


    private void NetworkDialog1() {
        final Dialog dialogs = new Dialog(EndShift.this);
        dialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogs.setContentView(R.layout.networkdialog);
        dialogs.setCanceledOnTouchOutside(false);
        Button done = (Button) dialogs.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogs.dismiss();
                Logout();

            }
        });
        dialogs.show();
    }
}