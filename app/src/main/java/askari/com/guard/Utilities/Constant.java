package askari.com.guard.Utilities;

public class Constant {

    /*
    1111111111
    1234
    */
    public static final String LOGIN_URL = "https://haringey.askaritechnologies.com/Api/login";
    public static final String GETDATA_URL = "https://haringey.askaritechnologies.com/Api/get_guards";
    public static final String SCAN_URL = "https://haringey.askaritechnologies.com/Api/scan_checkpoint";
    public static final String ABOUT_URL = "https://haringey.askaritechnologies.com/Api/about_us";
    public static final String ATTENDANCE_URL = "https://haringey.askaritechnologies.com/Api/get_attendance";
    public static final String MISSEDPT_URL = "https://haringey.askaritechnologies.com/Api/get_missed";
    public static final String LOGOUT_URL = "https://haringey.askaritechnologies.com/Api/sign_out";
    public static final String FORGETPASSWORD_URL = "https://haringey.askaritechnologies.com/Api/forget_password";
    public static final String ROUND_URL = "https://haringey.askaritechnologies.com/Api/get_rounds";
    public static final String HISTORY_URL = "https://haringey.askaritechnologies.com/Api/r_history";
    public static final String DETAILED_URL = "https://haringey.askaritechnologies.com/Api/r_history_detailed";
    public static final String HISTORYSORT_URL = "https://haringey.askaritechnologies.com/Api/history_by_date";

    //    custom code
    public static final String POLICY_URL = "https://demo.askaritechnologies.com/Api/policy";

    public static final String PHONE_URL = "https://haringey.askaritechnologies.com/Api/phone_info";
    public static final String ACCESS_CODE = "https://haringey.askaritechnologies.com/Api/access_code";
    public static final String BOOK_OFF = "https://haringey.askaritechnologies.com/Api/Book_off";
    public static final String BOOK_OFF1 = "https://haringey.askaritechnologies.com/Api/Book_off1";

    public static final String LEAVE = "https://haringey.askaritechnologies.com/Api/leave_request";
    public static final String DDOB = "https://haringey.askaritechnologies.com/Api/ddob";
    public static final String INCIDENT = "https://haringey.askaritechnologies.com/Api/incident";
    public static final String LEAVE_HISTORY = "https://haringey.askaritechnologies.com/Api/leave_history";
    public static final String RET = "https://haringey.askaritechnologies.com/Ret/img";






    /*public static final String LOGIN_URL = "https://haringey.askaritechnologies.com/Api/login";
    public static final String GETDATA_URL = "https://haringey.askaritechnologies.com/Api/get_guards";
    public static final String SCAN_URL = "https://haringey.askaritechnologies.com/Api/scan_checkpoint";
    public static final String ABOUT_URL = "https://haringey.askaritechnologies.com/Api/about_us";
    public static final String ATTENDANCE_URL = "https://haringey.askaritechnologies.com/Api/get_attendance";
    public static final String MISSEDPT_URL = "https://haringey.askaritechnologies.com/Api/get_missed";
    public static final String LOGOUT_URL = "https://haringey.askaritechnologies.com/Api/sign_out";
    public static final String FORGETPASSWORD_URL = "https://haringey.askaritechnologies.com/Api/forget_password";
    public static final String ROUND_URL = "https://haringey.askaritechnologies.com/Api/get_rounds";
    public static final String HISTORY_URL = "https://haringey.askaritechnologies.com/Api/r_history";
    public static final String DETAILED_URL = "https://haringey.askaritechnologies.com/Api/r_history_detailed";
    public static final String HISTORYSORT_URL = "https://haringey.askaritechnologies.com/Api/history_by_date";

//    custom code
    public static final String POLICY_URL = "https://demo.askaritechnologies.com/Api/policy";

    public static final String PHONE_URL = "https://haringey.askaritechnologies.com/Api/phone_info";
    public static final String ACCESS_CODE = "https://haringey.askaritechnologies.com/Api/access_code";
    public static final String BOOK_OFF = "https://haringey.askaritechnologies.com/Api/Book_off";
    public static final String BOOK_OFF1 = "https://haringey.askaritechnologies.com/Api/Book_off1";

    public static final String LEAVE = "https://haringey.askaritechnologies.com/Api/leave_request";
    public static final String DDOB = "https://haringey.askaritechnologies.com/Api/ddob";
    public static final String INCIDENT = "https://haringey.askaritechnologies.com/Api/incident";
    public static final String LEAVE_HISTORY = "https://haringey.askaritechnologies.com/Api/leave_history";
    public static final String RET = "https://haringey.askaritechnologies.com/Ret/img";*/

}